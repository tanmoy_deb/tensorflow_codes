import tensorflow as tf


#Define computation graph


#create input nodes
#tf.float32 --> data type

X = tf.placeholder(tf.float32, name="x")
Y = tf.placeholder(tf.float32, name="y")

#operation node
#This node adds two sensors X, Y which passed to it
addition = tf.add(X, Y, name="addition")

#create the session to run the computation model
with tf.Session() as sess:
    result = sess.run(addition, feed_dict={X: [5,3,1], Y: [10,6,1]})
    
    #create a event file in the dictionary and add graph to it
    writer = tf.summary.FileWriter("C:/Users/Admin/eclipse-workspace/5_tensor_graph/logs/add", sess.graph)
    print(result)
    
#run the command $tensorboard --logdir=logs

